const buttonText = document.getElementById('buttonText');
const text = document.getElementById('text');

const buttonImage = document.getElementById('buttonImage');
const image = document.getElementById('image');

buttonText.addEventListener('click', () => {
  const changeText = () => {
    text.innerHTML = `Text ${Date.now()}`;
  }

  document.startViewTransition(() => changeText());
})

buttonImage.addEventListener('click', () => {
  const changeImage = () => {
    image.src = `https://i1.sndcdn.com/artworks-C2kpp1uQzkqo7Dgx-37z13w-t500x500.jpg`;
  }

  document.startViewTransition(() => changeImage());
})


